import { test, expect } from "@playwright/test";

test.describe("[SCRIPT] apps", () => {
  test.describe("[GET] /v0.1/apps", () => {
    test("get app all OK", async ({ request }) => {
      const userInformation = await request.get("/v0.1/apps");
      expect(userInformation.ok()).toBeTruthy();
      expect(userInformation.status()).toEqual(200);
      expect(await userInformation.json()).toEqual(
        expect.arrayContaining([
          {
            id: "ff5b47c2-603a-4c6e-bb61-0c06880632d0",
            app_secret: "56627934-7fd7-4644-a4e3-604cd8b35a3a",
            description: null,
            display_name: "test t",
            name: "test-t",
            os: "iOS",
            platform: "React-Native",
            origin: "appcenter",
            icon_url: null,
            created_at: "2023-04-25T01:21:01.728Z",
            updated_at: "2023-04-25T01:21:01.728Z",
            release_type: "Production",
            owner: {
              id: "d35b50d8-4000-49a2-b4d6-f3557efe69bb",
              avatar_url: null,
              display_name: "CARLOS ANDRES CASTILLA GARCIA",
              email: "carloscastilla31@gmail.com",
              name: "carloscastilla31-gmail.com",
              type: "user",
            },
            azure_subscription: null,
            member_permissions: ["manager"],
          },
          {
            id: "6139bdab-1cef-410f-82c9-af20c0bc85e1",
            app_secret: "4922d32d-752a-48bc-9f0a-9056067fe082",
            description: null,
            display_name: "OpenApi",
            name: "OpenApi",
            os: "Android",
            platform: "Xamarin",
            origin: "appcenter",
            icon_url: null,
            created_at: "2023-04-22T16:11:18.471Z",
            updated_at: "2023-04-22T16:11:18.471Z",
            release_type: "Beta",
            owner: {
              id: "d35b50d8-4000-49a2-b4d6-f3557efe69bb",
              avatar_url: null,
              display_name: "CARLOS ANDRES CASTILLA GARCIA",
              email: "carloscastilla31@gmail.com",
              name: "carloscastilla31-gmail.com",
              type: "user",
            },
            azure_subscription: null,
            member_permissions: ["manager"],
          },
        ])
      );
    });

    test("get app all OK with parameters", async ({ request }) => {
      const userInformation = await request.get("/v0.1/apps?$orderBy=name");
      expect(userInformation.ok()).toBeTruthy();
      expect(userInformation.status()).toEqual(200);
      expect(await userInformation.json()).toEqual(
        expect.arrayContaining([
          {
            id: "6139bdab-1cef-410f-82c9-af20c0bc85e1",
            app_secret: "4922d32d-752a-48bc-9f0a-9056067fe082",
            description: null,
            display_name: "OpenApi",
            name: "OpenApi",
            os: "Android",
            platform: "Xamarin",
            origin: "appcenter",
            icon_url: null,
            created_at: "2023-04-22T16:11:18.471Z",
            updated_at: "2023-04-22T16:11:18.471Z",
            release_type: "Beta",
            owner: {
              id: "d35b50d8-4000-49a2-b4d6-f3557efe69bb",
              avatar_url: null,
              display_name: "CARLOS ANDRES CASTILLA GARCIA",
              email: "carloscastilla31@gmail.com",
              name: "carloscastilla31-gmail.com",
              type: "user",
            },
            azure_subscription: null,
            member_permissions: ["manager"],
          },
          {
            id: "ff5b47c2-603a-4c6e-bb61-0c06880632d0",
            app_secret: "56627934-7fd7-4644-a4e3-604cd8b35a3a",
            description: null,
            display_name: "test t",
            name: "test-t",
            os: "iOS",
            platform: "React-Native",
            origin: "appcenter",
            icon_url: null,
            created_at: "2023-04-25T01:21:01.728Z",
            updated_at: "2023-04-25T01:21:01.728Z",
            release_type: "Production",
            owner: {
              id: "d35b50d8-4000-49a2-b4d6-f3557efe69bb",
              avatar_url: null,
              display_name: "CARLOS ANDRES CASTILLA GARCIA",
              email: "carloscastilla31@gmail.com",
              name: "carloscastilla31-gmail.com",
              type: "user",
            },
            azure_subscription: null,
            member_permissions: ["manager"],
          },
        ])
      );
    });

    test("get all app query parameter is invalid ", async ({ request }) => {
      const userInformation = await request.get("/v0.1/apps?$orderBy=name1");
      expect(userInformation.ok()).toBeFalsy();
      expect(userInformation.status()).toEqual(400);
      expect(await userInformation.json()).toEqual(
        expect.objectContaining({
          error: {
            code: "BadRequest",
            message:
              'The "$orderBy" query parameter is invalid ("name1") \n' +
              "JSON Schema validation error. \n" +
              'No enum match for: "name1"',
          },
        })
      );
    });
  });
});
