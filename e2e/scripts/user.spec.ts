import { test, expect } from "@playwright/test";

test.describe("[SCRIPT] users", () => {
  test.describe("[GET] /v0.1/user", () => {
    test("Get User information", async ({ request }) => {
      const userInformation = await request.get("/v0.1/user");
      const expectedUserInformation = {
        id: "d35b50d8-4000-49a2-b4d6-f3557efe69bb",
        display_name: "CARLOS ANDRES CASTILLA GARCIA",
        email: "carloscastilla31@gmail.com",
        name: "carloscastilla31-gmail.com",
        avatar_url: null,
        can_change_password: false,
        created_at: "2023-04-22T16:00:17.454Z",
        origin: "appcenter",
      };

      const dataUserInformation = await userInformation.json();

      expect(userInformation.ok()).toBeTruthy();
      expect(userInformation.status()).toEqual(200);
      expect(dataUserInformation).toEqual(
        expect.objectContaining(expectedUserInformation)
      );
    });

    test("Get User information not token", async ({ request }) => {
      const userInformation = await request.get("/v0.1/user", {
        headers: { "X-API-Token": "" },
      });
      const dataUserInformation = await userInformation.json();

      expect(userInformation.ok()).toBeFalsy();
      expect(userInformation.status()).toEqual(401);
      expect(dataUserInformation).toEqual(
        expect.objectContaining({
          statusCode: 401,
          code: "Unauthorized",
        })
      );
    });

    test("Get User information Fails token invalid", async ({ request }) => {
      const userInformation = await request.get("/v0.1/user", {
        headers: { "X-API-Token": "50dc28d22ad94e0b859f7dc7fe7474eabc4e13081" },
      });
      expect(userInformation.ok()).toBeFalsy();
      expect(userInformation.status()).toEqual(401);
      expect(await userInformation.json()).toEqual(
        expect.objectContaining({
          statusCode: 401,
          code: "Unauthorized",
        })
      );
    });
  });

  test.describe("[PATCH] /v0.1/user", () => {
    test("Update displya name", async ({ request }) => {
      const userInformation = await request.patch("/v0.1/user", {
        data: {
          display_name: "CARLOS ANDRES CASTILLA GARCIA",
        },
      });

      expect(userInformation.ok()).toBeTruthy();
      expect(userInformation.status()).toEqual(200);
      expect(await userInformation.json()).toEqual(
        expect.objectContaining({
          id: "d35b50d8-4000-49a2-b4d6-f3557efe69bb",
          display_name: "CARLOS ANDRES CASTILLA GARCIA",
          email: "carloscastilla31@gmail.com",
          name: "carloscastilla31-gmail.com",
          avatar_url: null,
          can_change_password: false,
          origin: "appcenter",
        })
      );
    });

    test("Update displya name alphanumeric", async ({ request }) => {
      const userInformation = await request.patch("/v0.1/user", {
        data: {
          display_name: "CARLOS ANDRES CASTILLA GARCIA @",
        },
      });

      expect(userInformation.ok()).toBeFalsy();
      expect(userInformation.status()).toEqual(400);
      expect(await userInformation.json()).toEqual(
        expect.objectContaining({
          error: {
            code: "BadRequest",
            message: "Name contains invalid characters     @",
          },
        })
      );
    });

    test("Update displya name long", async ({ request }) => {
      const userInformation = await request.patch("/v0.1/user", {
        data: {
          display_name:
            "CARLOS ANDRES CASTILLA GARCIACARLOS ANDRES CASTILLA GARCIACARLOS ANDRES CASTILLA GARCIACARLOS ANDRES CASTILLA GARCIACARLOS ANDRES CASTILLA GARCIACARLOS ANDRES CASTILLA GARCIACARLOS ANDRES CASTILLA GARCIA",
        },
      });
      expect(userInformation.ok()).toBeFalsy();
      expect(userInformation.status()).toEqual(400);
      expect(await userInformation.json()).toEqual(
        expect.objectContaining({
          error: {
            code: "BadRequest",
            message: "Validation error: Validation len on display_name failed",
          },
        })
      );
    });
  });
});
