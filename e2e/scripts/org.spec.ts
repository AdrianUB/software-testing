import { test, expect } from "@playwright/test";

test.describe("[SCRIPT] orgs ", () => {
  test.describe("[GET] /v0.1/orgs ", () => {
    test("get orgs all OK", async ({ request }) => {
      const userInformation = await request.get("/v0.1/orgs");
      expect(userInformation.ok()).toBeTruthy();
      expect(userInformation.status()).toEqual(200);
      expect(await userInformation.json()).toEqual(
        expect.arrayContaining([
          {
            id: "3258855c-e2f2-4a60-911b-edf195de26ed",
            display_name: "schoolS",
            name: "schools",
            origin: "appcenter",
            created_at: "2023-04-25T01:17:58.026Z",
            updated_at: "2023-04-25T01:17:58.026Z",
            avatar_url: null,
          },
          {
            id: "34a036e9-b1ed-4e3b-bb61-bc63db44881d",
            display_name: "schoolSx",
            name: "schoolsx",
            origin: "appcenter",
            created_at: "2023-04-25T01:47:15.071Z",
            updated_at: "2023-04-25T01:47:15.071Z",
            avatar_url: null,
          },
        ])
      );
    });

    test("get orgs all not token", async ({ request }) => {
      const userInformation = await request.get("/v0.1/orgs", {
        headers: { "X-API-Token": "" },
      });
      expect(userInformation.ok()).toBeFalsy();
      expect(userInformation.status()).toEqual(401);
      expect(await userInformation.json()).toEqual(
        expect.objectContaining({
          statusCode: 401,
          code: "Unauthorized",
        })
      );
    });

    test("get orgs all with token fails", async ({ request }) => {
      const userInformation = await request.get("/v0.1/orgs", {
        headers: { "X-API-Token": "xxxxxxxxxxx" },
      });
      expect(userInformation.ok()).toBeFalsy();
      expect(userInformation.status()).toEqual(401);
      expect(await userInformation.json()).toEqual(
        expect.objectContaining({
          statusCode: 401,
          code: "Unauthorized",
        })
      );
    });
  });

  test.describe("[POST] /v0.1/orgs ", () => {
    test("Save org fails name is not available", async ({ request }) => {
      const userInformation = await request.post("/v0.1/orgs", {
        data: {
          display_name: "schoolSX",
          name: "schoolsX",
        },
      });

      const dataUserInformation = await userInformation.json();
      expect(userInformation.ok()).toBeFalsy();
      expect(userInformation.status()).toEqual(429);
      expect(dataUserInformation).toEqual(
        expect.objectContaining({
          error: {
            code: "Too Many Requests",
            message: "You created too many Organizations. Please wait a while.",
          },
        })
      );
    });

    test("Save org Ok", async ({ request }) => {
      const userInformation = await request.post("/v0.1/orgs", {
        data: {
          display_name: generateRandomString(6),
          name: generateRandomString(6),
        },
      });

      expect(userInformation.ok()).toBeFalsy();
      expect(userInformation.status()).toEqual(429);
      expect(await userInformation.json()).toEqual(
        expect.objectContaining({
          error: {
            code: "Too Many Requests",
            message: "You created too many Organizations. Please wait a while.",
          },
        })
      );
    });

    test("Save org Fails not token", async ({ request }) => {
      const userInformation = await request.post("/v0.1/orgs", {
        data: {
          display_name: generateRandomString(6),
          name: generateRandomString(6),
        },
        headers: { "X-API-Token": "" },
      });
      expect(userInformation.ok()).toBeFalsy();
      expect(userInformation.status()).toEqual(401);
      expect(await userInformation.json()).toEqual(
        expect.objectContaining({
          statusCode: 401,
          code: "Unauthorized",
        })
      );
    });

    test("Save org Fails token fails", async ({ request }) => {
      const userInformation = await request.post("/v0.1/orgs", {
        data: {
          display_name: generateRandomString(6),
          name: generateRandomString(6),
        },
        headers: { "X-API-Token": "xxxxxxxxxxxx" },
      });
      expect(userInformation.ok()).toBeFalsy();
      expect(userInformation.status()).toEqual(401);
      expect(await userInformation.json()).toEqual(
        expect.objectContaining({
          statusCode: 401,
          code: "Unauthorized",
        })
      );
    });
  });
});

function generateRandomString(length: number) {
  let result = "";
  const characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * characters.length));
  }
  return result;
}
