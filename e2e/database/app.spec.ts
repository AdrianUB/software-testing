import { test, expect } from "@playwright/test";
import prisma from "../helpers/prisma";

test.describe("[DATABASE] apps", () => {
  test.describe("[GET] /v0.1/apps", () => {
    test("get app all OK", async ({ request }) => {
      const userInformation = await request.get("/v0.1/apps");
      const expectedUserInformation = await prisma.tesPlan.findFirstOrThrow({
        where: {
          name: "apps.get.all",
          path: "/v0.1/apps",
          method: "GET",
        },
      });
      expect(userInformation.ok()).toEqual(expectedUserInformation.ok);
      expect(userInformation.status()).toEqual(
        expectedUserInformation.statusResponse
      );
      expect(await userInformation.json()).toEqual(
        expect.arrayContaining(expectedUserInformation.response as any[])
      );
    });

    test("get app all OK with parameters", async ({ request }) => {
      const userInformation = await request.get("/v0.1/apps?$orderBy=name");
      const expectedUserInformation = await prisma.tesPlan.findFirstOrThrow({
        where: {
          name: "apps.get.all.parameters",
          path: "/v0.1/apps",
          method: "GET",
        },
      });
      expect(userInformation.ok()).toEqual(expectedUserInformation.ok);
      expect(userInformation.status()).toEqual(
        expectedUserInformation.statusResponse
      );
      expect(await userInformation.json()).toEqual(
        expect.arrayContaining(expectedUserInformation.response as any[])
      );
    });

    test("get all app query parameter is invalid ", async ({ request }) => {
      const userInformation = await request.get("/v0.1/apps?$orderBy=name1");
      const expectedUserInformation = await prisma.tesPlan.findFirstOrThrow({
        where: {
          name: "apps.get.all.parameters.invalid",
          path: "/v0.1/apps",
          method: "GET",
        },
      });
      expect(userInformation.ok()).toEqual(expectedUserInformation.ok);
      expect(userInformation.status()).toEqual(
        expectedUserInformation.statusResponse
      );
      const response = await userInformation.json();
      delete response.error.message;

      expect(response).toEqual(
        expect.objectContaining(expectedUserInformation.response as any)
      );
    });
  });
});
