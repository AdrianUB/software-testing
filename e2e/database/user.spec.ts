import { test, expect } from "@playwright/test";
import prisma from "../helpers/prisma";

test.describe("[DATABASE] users", () => {
  test.describe("[GET] /v0.1/user", () => {
    test("Get User information", async ({ request }) => {
      const userInformation = await request.get("/v0.1/user");
      const expectedUserInformation = await prisma.tesPlan.findFirstOrThrow({
        where: {
          name: "users.get.success",
          path: "/v0.1/user",
          method: "GET",
        },
      });

      const dataUserInformation = await userInformation.json();

      expect(userInformation.ok()).toEqual(expectedUserInformation.ok);
      expect(userInformation.status()).toEqual(
        expectedUserInformation.statusResponse
      );
      expect(dataUserInformation).toEqual(
        expect.objectContaining(expectedUserInformation.response as any)
      );
    });

    test("Get User information not token", async ({ request }) => {
      const userInformation = await request.get("/v0.1/user", {
        headers: { "X-API-Token": "" },
      });
      const expectedUserInformation = await prisma.tesPlan.findFirstOrThrow({
        where: {
          name: "users.get.without-token",
          path: "/v0.1/user",
          method: "GET",
        },
      });

      const dataUserInformation = await userInformation.json();

      expect(userInformation.ok()).toEqual(expectedUserInformation.ok);
      expect(userInformation.status()).toEqual(
        expectedUserInformation.statusResponse
      );
      expect(dataUserInformation).toEqual(
        expect.objectContaining(expectedUserInformation.response as any)
      );
    });

    test("Get User information Fails token invalid", async ({ request }) => {
      const userInformation = await request.get("/v0.1/user", {
        headers: { "X-API-Token": "XXXXXXXXXXXXXXXX" },
      });
      const expectedUserInformation = await prisma.tesPlan.findFirstOrThrow({
        where: {
          name: "users.get.invalid-token",
          path: "/v0.1/user",
          method: "GET",
        },
      });
      expect(userInformation.ok()).toEqual(expectedUserInformation.ok);
      expect(userInformation.status()).toEqual(
        expectedUserInformation.statusResponse
      );
      expect(await userInformation.json()).toEqual(
        expect.objectContaining(expectedUserInformation.response as any)
      );
    });
  });

  test.describe("[PATCH] /v0.1/user", () => {
    test("Update displya name", async ({ request }) => {
      const userInformation = await request.patch("/v0.1/user", {
        data: {
          display_name: "CARLOS ANDRES CASTILLA GARCIA",
        },
      });

      const expectedUserInformation = await prisma.tesPlan.findFirstOrThrow({
        where: {
          name: "users.patch.display_name.success",
          path: "/v0.1/user",
          method: "GET",
        },
      });

      expect(userInformation.ok()).toEqual(expectedUserInformation.ok);
      expect(userInformation.status()).toEqual(
        expectedUserInformation.statusResponse
      );
      expect(await userInformation.json()).toEqual(
        expect.objectContaining(expectedUserInformation.response as any)
      );
    });

    test("Update displya name alphanumeric", async ({ request }) => {
      const userInformation = await request.patch("/v0.1/user", {
        data: {
          display_name: "CARLOS ANDRES CASTILLA GARCIA @",
        },
      });

      const expectedUserInformation = await prisma.tesPlan.findFirstOrThrow({
        where: {
          name: "users.patch.display_name.invalid-characters",
          path: "/v0.1/user",
          method: "GET",
        },
      });

      expect(userInformation.ok()).toEqual(expectedUserInformation.ok);
      expect(userInformation.status()).toEqual(
        expectedUserInformation.statusResponse
      );
      expect(await userInformation.json()).toEqual(
        expect.objectContaining(expectedUserInformation.response as any)
      );
    });

    test("Update displya name long", async ({ request }) => {
      const userInformation = await request.patch("/v0.1/user", {
        data: {
          display_name:
            "CARLOS ANDRES CASTILLA GARCIACARLOS ANDRES CASTILLA GARCIACARLOS ANDRES CASTILLA GARCIACARLOS ANDRES CASTILLA GARCIACARLOS ANDRES CASTILLA GARCIACARLOS ANDRES CASTILLA GARCIACARLOS ANDRES CASTILLA GARCIA",
        },
      });

      const expectedUserInformation = await prisma.tesPlan.findFirstOrThrow({
        where: {
          name: "users.patch.display_name.long",
          path: "/v0.1/user",
          method: "GET",
        },
      });

      expect(userInformation.ok()).toEqual(expectedUserInformation.ok);
      expect(userInformation.status()).toEqual(
        expectedUserInformation.statusResponse
      );
      expect(await userInformation.json()).toEqual(
        expect.objectContaining(expectedUserInformation.response as any)
      );
    });
  });
});
