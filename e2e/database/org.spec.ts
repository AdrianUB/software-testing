import { test, expect } from "@playwright/test";
import prisma from "../helpers/prisma";

test.describe("[DATABASE] orgs ", () => {
  test.describe("[GET] /v0.1/orgs ", () => {
    test("get orgs all OK", async ({ request }) => {
      const userInformation = await request.get("/v0.1/orgs");
      const expectedUserInformation = await prisma.tesPlan.findFirstOrThrow({
        where: {
          name: "orgs.get.all",
          path: "/v0.1/orgs",
          method: "GET",
        },
      });
      expect(userInformation.ok()).toEqual(expectedUserInformation.ok);
      expect(userInformation.status()).toEqual(
        expectedUserInformation.statusResponse
      );
      expect(await userInformation.json()).toEqual(
        expect.arrayContaining(expectedUserInformation.response as any[])
      );
    });

    test("get orgs all not token", async ({ request }) => {
      const userInformation = await request.get("/v0.1/orgs", {
        headers: { "X-API-Token": "" },
      });
      const expectedUserInformation = await prisma.tesPlan.findFirstOrThrow({
        where: {
          name: "orgs.get.all.without-token",
          path: "/v0.1/orgs",
          method: "GET",
        },
      });
      expect(userInformation.ok()).toEqual(expectedUserInformation.ok);
      expect(userInformation.status()).toEqual(
        expectedUserInformation.statusResponse
      );
      expect(await userInformation.json()).toEqual(
        expect.objectContaining(expectedUserInformation.response as any)
      );
    });

    test("get orgs all with token fails", async ({ request }) => {
      const userInformation = await request.get("/v0.1/orgs", {
        headers: { "X-API-Token": "xxxxxxxxxxx" },
      });
      const expectedUserInformation = await prisma.tesPlan.findFirstOrThrow({
        where: {
          name: "orgs.get.all.with-invalid-token",
          path: "/v0.1/orgs",
          method: "GET",
        },
      });
      expect(userInformation.ok()).toEqual(expectedUserInformation.ok);
      expect(userInformation.status()).toEqual(
        expectedUserInformation.statusResponse
      );
      expect(await userInformation.json()).toEqual(
        expect.objectContaining(expectedUserInformation.response as any)
      );
    });
  });

  test.describe("[POST] /v0.1/orgs ", () => {
    test("Save org fails name is not available", async ({ request }) => {
      const userInformation = await request.post("/v0.1/orgs", {
        data: {
          display_name: "schoolSX",
          name: "schoolsX",
        },
      });

      const expectedUserInformation = await prisma.tesPlan.findFirstOrThrow({
        where: {
          name: "orgs.post.save.fail.many-organizations",
          path: "/v0.1/orgs",
          method: "POST",
        },
      });

      const dataUserInformation = await userInformation.json();
      expect(userInformation.ok()).toEqual(expectedUserInformation.ok);
      expect(userInformation.status()).toEqual(
        expectedUserInformation.statusResponse
      );
      expect(dataUserInformation).toEqual(
        expect.objectContaining(expectedUserInformation.response as any)
      );
    });

    test("Save org Ok", async ({ request }) => {
      const userInformation = await request.post("/v0.1/orgs", {
        data: {
          display_name: generateRandomString(6),
          name: generateRandomString(6),
        },
      });
      const expectedUserInformation = await prisma.tesPlan.findFirstOrThrow({
        where: {
          name: "orgs.post.save.fail.many-organizations",
          path: "/v0.1/orgs",
          method: "POST",
        },
      });
      expect(userInformation.ok()).toEqual(expectedUserInformation.ok);
      expect(userInformation.status()).toEqual(
        expectedUserInformation.statusResponse
      );
      expect(await userInformation.json()).toEqual(
        expect.objectContaining(expectedUserInformation.response as any)
      );
    });

    test("Save org Fails not token", async ({ request }) => {
      const userInformation = await request.post("/v0.1/orgs", {
        data: {
          display_name: generateRandomString(6),
          name: generateRandomString(6),
        },
        headers: { "X-API-Token": "" },
      });

      const expectedUserInformation = await prisma.tesPlan.findFirstOrThrow({
        where: {
          name: "orgs.post.save.fail.without-token",
          path: "/v0.1/orgs",
          method: "POST",
        },
      });

      expect(userInformation.ok()).toEqual(expectedUserInformation.ok);
      expect(userInformation.status()).toEqual(
        expectedUserInformation.statusResponse
      );
      expect(await userInformation.json()).toEqual(
        expect.objectContaining(expectedUserInformation.response as any)
      );
    });

    test("Save org Fails token fails", async ({ request }) => {
      const userInformation = await request.post("/v0.1/orgs", {
        data: {
          display_name: generateRandomString(6),
          name: generateRandomString(6),
        },
        headers: { "X-API-Token": "xxxxxxxxxxxx" },
      });

      const expectedUserInformation = await prisma.tesPlan.findFirstOrThrow({
        where: {
          name: "orgs.post.save.fail.invalid-token",
          path: "/v0.1/orgs",
          method: "POST",
        },
      });

      expect(userInformation.ok()).toEqual(expectedUserInformation.ok);
      expect(userInformation.status()).toEqual(
        expectedUserInformation.statusResponse
      );
      expect(await userInformation.json()).toEqual(
        expect.objectContaining(expectedUserInformation.response as any)
      );
    });
  });
});

function generateRandomString(length: number) {
  let result = "";
  const characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * characters.length));
  }
  return result;
}
