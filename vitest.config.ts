import { defineConfig } from "vitest/config";

export default defineConfig({
  test: {
    threads: false,
    testTimeout: 999999,
    include: ["./src/**/*.test.ts"],
    reporters: ["html"],
  },
});
